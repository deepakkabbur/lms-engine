$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "course_creation/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "course_creation"
  s.version     = CourseCreation::VERSION
  s.authors     = ["weboniselab"]
  s.email       = ["lfe@weboniselab.com"]
  s.homepage    = "http://weboniselab.com"
  s.summary     = "CourseCreation."
  s.description = "Course Creation is Rails engine which provides course creation functionalities "
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.1"
end
